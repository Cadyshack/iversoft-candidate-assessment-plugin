<?php
function iversoft_candidate() {
	$labels = array(
    'name'               => 'Candidate',
    'singular_name'      => 'Candidate',
    'menu_name'          => 'Candidates',
    'name_admin_bar'     => 'Candidates',
    'add_new'            => 'Add New',
    'add_new_item'       => 'Add New Candidate',
    'new_item'           => 'New Candidate',
    'edit_item'          => 'Edit Candidate',
    'view_item'          => 'View Candidate',
    'all_items'          => 'All Candidates',
    'search_items'       => 'Search Candidates',
    'parent_item_colon'  => 'Parent Candidates:',
    'not_found'          => 'No Candidates found.',
    'taxonomies'         => array('jobs'),
    'not_found_in_trash' => 'No Candidates found in Trash.',
  );

  $args = array(
    'labels'             => $labels,
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'has_archive'        => false,
    'show_in_menu'       => true,
    'menu_icon'          => 'dashicons-images-alt2',
    'hierarchical'       => false,
    'menu_position'      => 5,
    'supports'           => array( 'title', 'editor', 'revisions', 'thumbnail', 'page-attributes' ),
  );
  register_post_type( 'candidate', $args );
}
add_action( 'init', 'iversoft_candidate');


function iversoft_job_posting() {
  // Solutions taxonomy (hierarchical)
  $labels = array(
    'name'                       => 'Open Position',
    'singular_name'              => 'Open Position',
    'search_items'               => 'Search Open Positions',
    'popular_items'              => 'Popular Open Positions',
    'all_items'                  => 'All Open Positions',
    'edit_item'                  => 'Edit Open Position',
    'update_item'                => 'Update Open Position',
    'parent_item'                => 'Parent Open Position',
    'parent_item_colon'          => 'Parent Open Position:',
    'add_new_item'               => 'Add New Open Position',
    'new_item_name'              => 'New Open Position Name',
    'not_found'                  => 'No Open Positions found.',
    'menu_name'                  => 'Open Positions',
  );

  $args = array(
    'labels'                => $labels,
    'hierarchical'          => true,
    'show_ui'               => true,
    'query_var'             => true,
    'show_in_menu'	=> true,
    'show_admin_column' => true,
  );

  register_taxonomy( 'jobs', array( 'candidate' ), $args );
}
add_action( 'init', 'iversoft_job_posting' );

// Flush rewrite rules to add "candidate" as a permalink slug
function iversoft_rewrite_flush() {
  iversoft_candidate();
  iversoft_job_posting();
  flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'iversoft_rewrite_flush' );