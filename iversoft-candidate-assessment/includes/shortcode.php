<?php
/**
 * Shortcode to show all the candidates
 */

function iversoft_candidate_shortcode( $atts ) {
  $a = shortcode_atts( array(
    'job' => null
  ), $atts);

  $args = array(
    'post_type' => 'candidate',
    'order' => 'ASC',
    'posts_per_page' => -1,
  );

  $candidate_query = new WP_Query( $args );

  //start output buffering
  ob_start(); ?>

  <div class="candidate-list">

      <?php
      while ( $candidate_query->have_posts() ):
        $candidate_query->the_post();
        global $post;
        $terms = get_the_terms( $post->ID, 'jobs');

        ?>
        <div class="candidate">
          <div class="name">
            <h1>Name: <?php the_title(); ?></h1>
          </div>
          <div class="jobs">
            <p><strong>Position of Interest:</strong></p>

            <?php
            foreach ( $terms as $term ): ?>
              <p><?php echo $term->name; ?></p>
              <?php
            endforeach;
             ?>

          </div><!-- .jobs -->
          <div class="skills">
            <h2>Skills</h2>
            <ul>
            <?php
            // iterate through skills repeater field
            if ( have_rows('skills') ):
              while ( have_rows('skills') ): the_row();
                $skill = get_sub_field('skill');
                $level = get_sub_field('skill_level');
                ?>
                <li><?php echo $skill . " ( $level )"; ?></li>
                <?php
              endwhile; // while ( have_rows('skills') ): the_row();

            endif; //if ( have_rows('skills') ):
             ?>
            </ul>
          </div>
        </div>
        <?php
      endwhile; // while ( $candidate_query->have_posts() ):
      wp_reset_postdata();
      ?>
  </div>
  <?php
  $candidates = ob_get_clean();
  return $candidates;
}

// wait until WordPress has initialized before adding shortcode
function iversoft_shortcodes_init(){
  add_shortcode('applicants', 'iversoft_candidate_shortcode');
}
add_action('init', 'iversoft_shortcodes_init');