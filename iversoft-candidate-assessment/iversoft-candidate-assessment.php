<?php
/*
Plugin Name: Iversoft Candidate Assessment
Description: Custom plugin built for Iversoft Solutions candidate assessment process.
Plugin URI:	 https://bitbucket.org/Cadyshack/iversoft-candidate-assessment-plugin
Author:      Christian Cadieux
Version:     1.0
License:     GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-3.0.txt

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version
2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
with this program. If not, visit: https://www.gnu.org/licenses/
*/

// load candidate custom post type and corresponding jobs custom taxonomy
require_once plugin_dir_path( __FILE__ ) . 'includes/posttypes.php';

// load shortcode file
require_once plugin_dir_path( __FILE__ ) . 'includes/shortcode.php';

// Load styles used for shortcode output
function iversoft_enqueue_style_public() {
  $src = plugin_dir_url( __FILE__ ) . 'public/css/iversoft-plugin-style.css';
  wp_enqueue_style( 'iversoft-styles-public', $src, array(), null, 'all');
}
add_action( 'wp_enqueue_scripts', 'iversoft_enqueue_style_public');




/**
 * Include Advanced Custom Field Pro in Plugin
 */
// If ACF Pro plugin does not currently exist then do the following
if ( ! class_exists('acf') ) {
  define('IV_ACF_PATH', plugin_dir_path( __FILE__ ) . 'includes/acf/' );
  define('IV_ACF_URL', plugin_dir_url( __FILE__ ) . 'includes/acf/');

  // Include the ACF plugin.
  include_once( IV_ACF_PATH . 'acf.php' );

  // Customize the url setting to fix incorrect asset URLs.

  function iversoft_acf_settings_url( $url ) {
    return IV_ACF_URL;
  }
  add_filter('acf/settings/url', 'iversoft_acf_settings_url');

  // Hide the ACF admin menu item
  add_filter('acf/settings/show_admin', '__return_false');

  // Load the Custom Fields associates with the candidate custom post type
  if( function_exists('acf_add_local_field_group') ) {

    acf_add_local_field_group(array(
      'key' => 'group_5ccf97d4c2b65',
      'title' => 'Candidate Skills',
      'fields' => array(
        array(
          'key' => 'field_5ccf97ff48092',
          'label' => 'Skills',
          'name' => 'skills',
          'type' => 'repeater',
          'instructions' => '',
          'required' => 0,
          'conditional_logic' => 0,
          'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
          ),
          'collapsed' => '',
          'min' => 0,
          'max' => 0,
          'layout' => 'block',
          'button_label' => '',
          'sub_fields' => array(
            array(
              'key' => 'field_5ccf981f48093',
              'label' => 'Skill',
              'name' => 'skill',
              'type' => 'text',
              'instructions' => 'Enter the skills that this candidate has shown to have',
              'required' => 0,
              'conditional_logic' => 0,
              'wrapper' => array(
                'width' => '50',
                'class' => '',
                'id' => '',
              ),
              'default_value' => '',
              'placeholder' => '',
              'prepend' => '',
              'append' => '',
              'maxlength' => '',
            ),
            array(
              'key' => 'field_5ccf985548094',
              'label' => 'Skill Level',
              'name' => 'skill_level',
              'type' => 'range',
              'instructions' => 'Input the level of expertise the candidate has related to this skill <br>
              1 = barely has this skill <br>
              10 = expert',
              'required' => 0,
              'conditional_logic' => 0,
              'wrapper' => array(
                'width' => '50',
                'class' => '',
                'id' => '',
              ),
              'default_value' => '',
              'min' => 1,
              'max' => 10,
              'step' => '',
              'prepend' => '',
              'append' => '',
            ),
          ),
        ),
      ),
      'location' => array(
        array(
          array(
            'param' => 'post_type',
            'operator' => '==',
            'value' => 'candidate',
          ),
        ),
      ),
      'menu_order' => 0,
      'position' => 'normal',
      'style' => 'default',
      'label_placement' => 'top',
      'instruction_placement' => 'label',
      'hide_on_screen' => '',
      'active' => true,
      'description' => '',
    ));

  }
}

?>