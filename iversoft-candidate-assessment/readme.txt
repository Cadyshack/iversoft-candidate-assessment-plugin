=== Plugin Name ===
Plugin Name: Iversoft Candidate Assessment
Description: Custom plugin built for Iversoft Solutions candidate assessment process.
Plugin URI:	 https://bitbucket.org/Cadyshack/iversoft-candidate-assessment-plugin
Author:      Christian Cadieux
Version:     1.0
License:     GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-3.0.txt

This plugin is created as a WordPress test for iversoft solutions, and is meant to be used to help assess potential candidates applying for a position.

== Description ==

= Decision to note =

One decision that was made that is not straightforward, and should be pointed out is how I decided to go about creating a meta box with a few custom fields.
Specifically, instead of creating the meta box with native WP functions, I used the Advanced Custom Field (ACF) plugin to create the meta box and custom fields.
I then included the ACF plugin source file in this plugin, and loaded the custom field via php found in the main pluign file at the bottom. This was done because ACF is a very well known plugin that allows developers to work faster, and more efficiently when creating custom fields, therefore using this method for this test was done to represent the workflow of most (if not all) professional developers.

= unforseen issues =

Only unforseen issue was the ability to add the ACF fields in the plugin via the local JSON method, which would be preferred to the PHP method used here. Unfortunately, the ability to sync the fields on plugin load was not as straighforward as expected, and documenation was lacking. Therefore the PHP method was used instead.

= items left unfinished =

* Would have liked to pull the candidate featured image when displaying list of candidate via shortcode
* Would also have liked to add some optional attributes to the shortcode to allow you to list candidates based on job opening or skills.
* An admin folder housing a 'css' sub folder would also be added to add some admin styles, as well as a admin settings section to add functionality to the plugin


