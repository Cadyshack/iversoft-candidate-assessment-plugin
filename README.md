# README #

The plugin is the 'iversoft-candidate-assessment' folder. Simply download this repository and load this folder, and only this folder inside the plugins folder found in **/wp-content/plugins/**.
Once this is done, activate the plugin through the 'Plugins' screen in WordPress.

## General Use ##

* Once activated, a 'Candidates' tab will appear on the left side.
* The Candidates tab will show a sub-menu item called 'Open Positions'.
	* This is where you add all the open positions at iversoft, which you can then associate to each candidate
* When adding candidates the title should be their name
* A custom meta box with the ability to add all the relevent skills the candidate possesses as well as the level of expertise they have for each skill will be present below the Content area titled 'Candidate Skills'
* The option to associate the candidate with an open position is found on the right in another meta box labeld 'Open Position'
* Add `[applicants]` shortcode anywhere you whish to have a list of applicants to show up.

### Decisions to Note ###

One decision that was made that is not straightforward, and should be pointed out is how I decided to go about creating a meta box with a few custom fields. Specifically, instead of creating the meta box with native WP functions, I used the Advanced Custom Field (ACF) plugin to create the meta box and custom fields. I then included the ACF plugin source file in this plugin, and loaded the custom field via php found in the main pluign file at the bottom. This was done because ACF is a very well known plugin that allows developers to work faster, and more efficiently when creating custom fields, therefore using this method for this test was done to represent the workflow of most (if not all) professional developers.

### Unforseen Issues ###

The only unforseen issue was the ability to add the ACF fields in the plugin via the local JSON method, which would be preferred to the PHP method used here. Unfortunately, the ability to sync the fields on plugin load was not as straighforward as expected, and documenation was lacking. Therefore the PHP method was used instead.

### Items Left Unfinished ###

* Would have liked to pull the candidate featured image when displaying list of candidate via shortcode
* Would also have liked to add some optional attributes to the shortcode to allow you to list candidates based on job opening or skills.
* An admin folder housing a 'css' sub folder would also be added to add some admin styles, as well as a admin settings section to add functionality to the plugin